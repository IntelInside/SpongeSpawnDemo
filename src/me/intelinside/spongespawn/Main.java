package me.intelinside.spongespawn;

import com.google.inject.Inject;
import me.intelinside.spongespawn.commands.CommandSpec;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.hocon.HoconConfigurationLoader;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.config.DefaultConfig;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GamePreInitializationEvent;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.world.Location;

import java.io.File;
import java.io.IOException;

@Plugin(id = "intelspongespawn", name = "IntelSpawn", version = "1.0")
public class Main {

    @Inject
    private Logger logger;

    private ConfigurationNode config;

    private Double spawnx;
    private Double spawny;
    private Double spawnz;
    private String spawnworld;

    private File configFile = new File("config\\intelspongespawn.conf");
    
    @Inject
    @DefaultConfig(sharedRoot = true)
    public ConfigurationLoader<CommentedConfigurationNode> configLoader = HoconConfigurationLoader.builder().setFile(configFile).build();
    
    @Listener
    public void onServerStart(GamePreInitializationEvent e) {
        logger.info("IntelSpawn is now enabling.", "Developed with love by IntelInside.");
        if (!configFile.exists()) {
            try {
                configFile.createNewFile();
                config = configLoader.load();

                config.getNode("spawn-x").setValue(0);
                config.getNode("spawn-y").setValue(0);
                config.getNode("spawn-z").setValue(0);
                config.getNode("spawn-world").setValue("thissupergenericworldnamewillbereplaced");

                configLoader.save(config);
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }

        if (config != null) {
            spawnx = config.getNode("spawn-x").getDouble();
            spawny = config.getNode("spawn-y").getDouble();
            spawnz = config.getNode("spawn-z").getDouble();
            spawnworld = config.getNode("spawn-world").getString();
        }
        Sponge.getCommandManager().register(this, (new CommandSpec()).getSpawnCommand(), "spawn");
        Sponge.getCommandManager().register(this, (new CommandSpec()).getSetSpawnCommand(), "setspawn");
        logger.info("IntelSpawn is now enabled.", "Developed with love by IntelInside.");
    }

    @Listener
    public void onServerStarted(GameStartedServerEvent e) {
        try {
            config = configLoader.load();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        if (config.getNode("spawn-world").getString().equalsIgnoreCase("thissupergenericworldnamewillbereplaced")) {
            config.getNode("spawn-world").setValue(Sponge.getServer().getDefaultWorld().get().getWorldName());
            logger.info("Config value with key of 'spawn-world' has been set to the default world name.");
            spawnworld = config.getNode("spawn-world").getString();
            try {
                configLoader.save(config);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        } else {
            logger.info("World in config found to be consistent with worlds in server, skipping replacement.");
        }
    }

    public Location returnSpawn() {
        return Sponge.getServer().getWorld(config.getNode("spawn-world").getString()).get()
                .getLocation(config.getNode("spawn-x").getDouble(),
                        config.getNode("spawn-y").getDouble(),
                        config.getNode("spawn-z").getDouble());
    }

    public ConfigurationNode returnConfig() {
        try {
            config = configLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return config;
    }

    public File returnConfigFile() {
        return this.configFile;
    }

    public void setSpawnPoints(double x, double y, double z) {
        spawnx = x;
        spawny = y;
        spawnz = z;
    } 
}
