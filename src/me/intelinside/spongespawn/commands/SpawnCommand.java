package me.intelinside.spongespawn.commands;

import me.intelinside.spongespawn.Main;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.hocon.HoconConfigurationLoader;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.io.IOException;

public class SpawnCommand implements CommandExecutor {
    private Main main = new Main();

    public ConfigurationLoader<CommentedConfigurationNode> configLoader = HoconConfigurationLoader.builder()
            .setFile(main.returnConfigFile()).build();

    @Override
    public CommandResult execute(CommandSource commandSource, CommandContext commandContext) throws CommandException {
        if (commandSource instanceof Player) {
            try {
                ConfigurationNode config = configLoader.load();
                ((Player) commandSource).setLocation(Sponge.getServer().getWorld(config.getNode("spawn-world").getString()).get()
                        .getLocation(config.getNode("spawn-x").getDouble(),
                                config.getNode("spawn-y").getDouble(),
                                config.getNode("spawn-z").getDouble()));

                commandSource.sendMessage(
                        Text.builder("Spawn ").color(TextColors.DARK_AQUA)
                                .append(Text.builder(">> ").color(TextColors.DARK_GRAY)
                                        .append(Text.builder("You have been teleported to spawn.").color(TextColors.GRAY).build()).build()).build()
                );
            } catch (IOException e) {
                e.printStackTrace();
                commandSource.sendMessage(Text.builder("ERROR ").color(TextColors.DARK_RED)
                        .append(Text.builder(">> ").color(TextColors.DARK_GRAY)
                                .append(Text.builder("A problem occured and your command could not be processed. (Administrators, please check console for stack trace. Report to developer.)").color(TextColors.GRAY)
                                        .build()).build()).build());
            }
        } else {
            commandSource.sendMessage(Text.of("You must be a player to execute this command."));
        }
        return CommandResult.success();
    }
}
