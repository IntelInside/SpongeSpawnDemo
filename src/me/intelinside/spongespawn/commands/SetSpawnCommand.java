package me.intelinside.spongespawn.commands;

import me.intelinside.spongespawn.Main;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.hocon.HoconConfigurationLoader;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandException;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.CommandSource;
import org.spongepowered.api.command.args.CommandContext;
import org.spongepowered.api.command.spec.CommandExecutor;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.Location;

import java.io.IOException;

public class SetSpawnCommand implements CommandExecutor {
    private Main main = new Main();

    public ConfigurationLoader<CommentedConfigurationNode> configLoader = HoconConfigurationLoader.builder()
            .setFile(main.returnConfigFile()).build();
    
    @Override
    public CommandResult execute(CommandSource commandSource, CommandContext commandContext) throws CommandException {
        if (commandSource instanceof Player) {
            try {
                ConfigurationNode configNode = configLoader.load();

                Location newSpawn = ((Player) commandSource).getLocation();
                Location oldSpawn = Sponge.getServer().getWorld(configNode.getNode("spawn-world").getString()).get().getLocation(configNode.getNode("spawn-x").getDouble(), configNode.getNode("spawn-y").getDouble(), configNode.getNode("spawn-z").getDouble());

                main.setSpawnPoints(newSpawn.getX(), newSpawn.getY(), newSpawn.getZ());

                commandSource.sendMessage(Text.builder("Spawn ").color(TextColors.DARK_AQUA)
                        .append(Text.builder(">> ").color(TextColors.DARK_GRAY)
                                .append(Text.builder("Spawn has been set to ").color(TextColors.GRAY)
                                        .append(Text.builder(newSpawn.getX() + " " + newSpawn.getY() + " " + newSpawn.getZ() + " ").color(TextColors.GREEN)
                                                .append(Text.builder("in the world ").color(TextColors.GRAY)
                                                        .append(Text.builder(((Player) commandSource).getWorld().getName()).color(TextColors.GREEN)
                                                                .append(Text.builder(". The previous spawn was ").color(TextColors.GRAY)
                                                                        .append(Text.builder(oldSpawn.getX() + " " + oldSpawn.getY() + " " + oldSpawn.getZ() + " ").color(TextColors.GREEN)
                                                                                .append(Text.builder("in the world ").color(TextColors.GRAY)
                                                                                        .append(Text.builder(configNode.getNode("spawn-world").getString()).color(TextColors.GREEN)
                                                                                                .append(Text.builder(".").color(TextColors.GRAY).build()
                                                                                                ).build()
                                                                                        ).build()
                                                                                ).build()
                                                                        ).build()
                                                                ).build()
                                                        ).build()
                                                ).build()
                                        ).build()
                                ).build()
                        ).build()
                );


                configNode.getNode("spawn-x").setValue(newSpawn.getX());
                configNode.getNode("spawn-y").setValue(newSpawn.getY());
                configNode.getNode("spawn-z").setValue(newSpawn.getZ());
                configNode.getNode("spawn-world").setValue(((Player) commandSource).getWorld().getName());

                configLoader.save(configNode);
            } catch (IOException e) {
                e.printStackTrace();
                commandSource.sendMessage(Text.builder("ERROR ").color(TextColors.DARK_RED)
                        .append(Text.builder(">> ").color(TextColors.DARK_GRAY)
                                .append(Text.builder("A problem occured and your command could not be processed. (Administrators, please check console for stack trace. Report to developer.)").color(TextColors.GRAY)
                                        .build()).build()).build());
            }

        } else {
            commandSource.sendMessage(Text.of("You must be a player to execute this command."));
        }
        return CommandResult.success();
    }
}
