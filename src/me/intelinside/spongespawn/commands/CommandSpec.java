package me.intelinside.spongespawn.commands;

import org.spongepowered.api.text.Text;

public class CommandSpec {
    private org.spongepowered.api.command.spec.CommandSpec spawnCommand = org.spongepowered.api.command.spec.CommandSpec.builder()
            .description(Text.of("Sends you to spawn."))
            .permission("spawn.use")
            .executor(new SpawnCommand())
            .build();

    private org.spongepowered.api.command.spec.CommandSpec setSpawnCommand = org.spongepowered.api.command.spec.CommandSpec.builder()
            .description(Text.of("Sets the spawn to your current location."))
            .permission("spawn.set")
            .executor(new SetSpawnCommand())
            .build();

    public org.spongepowered.api.command.spec.CommandSpec getSpawnCommand() {
        return spawnCommand;
    }

    public org.spongepowered.api.command.spec.CommandSpec getSetSpawnCommand() {
        return setSpawnCommand;
    }
}
