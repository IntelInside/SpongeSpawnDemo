This is a spawn plugin developed by IntelInside for demos.
This is the version developed for the Sponge platform. For Bukkit / Spigot, please follow this link. https://gitlab.com/IntelInside/SpawnDemo
It contains some nice features, such as:
/spawn (Obviously, as this functions as the main command.)
/setspawn (Change the spawn with a command instead of modifying the configuration file.)
Ability to change spawn through configuration.
Command Reference:
/spawn, Teleports to spawn, "spawn.use"
/setspawn, Sets the spawn, "spawn.set"

A compiled version is available for download at this link. https://www.mediafire.com/?lzbhdkl480l9mfs
A .zip file with a SpongeVanilla Server containing the plugin is available for download at this link. http://www.mediafire.com/download/z7njrxu6drfnl5e/SpongeVanilla_Server.zip

If you have any problems, please don't hesitate to contact me on Skype! My username is 'montvious'.